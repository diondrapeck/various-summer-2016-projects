""" All functions assume that lst is a list of integers or floats and that k 
    is a positive int representing the number of desired clusters."""
    
def poormans_kmeans(lst, k):
    import random
    
    if k >= len(lst):
        raise ValueError("Number of clusters must be less than list length")
        
    centroids = {}
    
    # randomly choose k items as initial centroids
    for i in range(0, k): 
        centroid = random.choice(lst)
        centroids[centroid] = [centroid]
        lst.remove(centroid)
    
    # assign each example to a cluster based on distance    
    for example in lst: 
        difference = []
        for centroid in centroids:
            difference.append((abs(example - centroid), example, centroid))
        cluster = min(difference)
        centroids[cluster[2]].append(cluster[1])
        
    return centroids
    
print poormans_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 2) 
print poormans_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 4)
print poormans_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 9)
        
def improved_kmeans(lst, k):
    import random
    import numpy as np
    
    if k >= len(lst):
        raise ValueError("Number of clusters must be less than list length")

    centroids = {}
    
    # randomly choose k items as initial centroids
    for i in range(0, k): 
        centroid = random.choice(lst)
        centroids[centroid] = [centroid]
        lst.remove(centroid)
    
    # assign each example to a cluster based on distance    
    for example in lst: 
        difference = []
        for centroid in centroids:
            difference.append((abs(example - centroid), example, centroid))
        cluster = min(difference)
        centroids[cluster[2]].append(cluster[1])
        avg = np.mean(centroids[cluster[2]])

        # updates centroids to mean of all cluster members
        if avg != cluster[2]:
            centroids[avg] = centroids.pop(cluster[2])
            
    return centroids  

# tests
print improved_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 2) 
print improved_kmeans([2, 443, 12, 642, 21], 5)    
print improved_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 4)   
print improved_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 9)        
        
def optimal_kmeans(lst, k, samples):
    import numpy as np
    import copy
    
    dissimilarities = []
    
    # produces number of requested kmeans results using given lst and k 
    for i in range(0, samples):
       examples = copy.copy(lst)
       sample = improved_kmeans(examples, k)
       dissimilarity = 0
       
       # calculates dissimilarity of each sample
       for cluster in sample.values():
           dissimilarity += np.var(cluster)
           dissimilarities.append((dissimilarity, sample))
         
    # return sample with least dissimilarity  
    return min(dissimilarities)[1]

# tests
print optimal_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 4, 100)
print optimal_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 4, 1000)
print optimal_kmeans([1, 2, 3, 1000, 200, 1001, 204, 1009, 4], 4, 10000)