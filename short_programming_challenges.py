''' 
Suppose we could access yesterday's stock prices as a list, where:

The indices are the time in minutes past trade opening time, which was 9:30am local time.
The values are the price in dollars of Apple stock at that time.
So if the stock cost $500 at 10:30am, stock_prices_yesterday[60] = 500.

Write an efficient function that takes stock_prices_yesterday and returns the best profit I could have made from 1 purchase and 1 sale of 1 Apple stock yesterday.

For example:

  stock_prices_yesterday = [10, 7, 5, 8, 11, 9]

get_max_profit(stock_prices_yesterday)
# returns 6 (buying for $5 and selling for $11)

No "shorting"—you must buy before you sell. You may not buy and sell in the same time step (at least 1 minute must pass).

Source: https://www.interviewcake.com/
'''

stock_prices_yesterday = [10, 7, 5, 8, 11, 9]
stock_prices_today = [20, 1, 2, 3, 4]
stock_neg = [32, 4, 3, 2, 1]

def get_max_profit(prices):
    
    info = [(price, prices.index(price))for price in prices]
    profit = 0
    
    for i in info:
        for j in info:
            if i == j or i[1] < j[1]:
                continue
            else:    
                dif = i[0] - j[0]
                if dif < 0:
                    continue
                if dif > profit:
                    profit = dif
                else:
                    continue
    return profit
                    
# unit tests 
assert get_max_profit(stock_prices_yesterday) == 6
assert get_max_profit(stock_prices_today) == 3
assert get_max_profit(stock_neg) == 0

'''
You have a list of integers, and for each index you want to find the product of every integer except the integer at that index.
Write a function get_products_of_all_ints_except_at_index() that takes a list of integers and returns a list of the products.

For example, given:

  [1, 7, 3, 4]

your function would return:

  [84, 12, 28, 21]

by calculating:

  [7*3*4, 1*3*4, 1*7*4, 1*7*3]

Do not use division in your solution.
Source: https://www.interviewcake.com/
'''

def product_skip(lst):
    from copy import copy
    
    prods = []
    for i in lst:
        c = copy(lst)
        c.remove(i)
        if 0 in c:
            prods.append(0)
        else:
            prods.append(reduce(lambda a, b: a*b, c))
    return prods

# unit tests
assert product_skip([0, 4, 2]) == [8, 0, 0]
assert product_skip([0, 0, 0, 0]) == [0, 0, 0, 0]
assert product_skip([1, 7, 3, 4]) == [84, 12, 28, 21]

# -*- coding: utf-8 -*-
'''A crack team of love scientists from OkEros (a hot new dating site) have devised a way to represent dating profiles as rectangles on a two-dimensional plane.
They need help writing an algorithm to find the intersection of two users' love rectangles. They suspect finding that intersection is the key to a matching algorithm so powerful it will cause an immediate acquisition by Google or Facebook or Obama or something.

Two rectangles overlapping a little. It must be love.
Write a function to find the rectangular intersection of two given love rectangles.

As with the example above, love rectangles are always "straight" and never "diagonal." More rigorously: each side is parallel with either the x-axis or the y-axis.

They are defined as dictionaries ↴ like this:

  my_rectangle = {

    # coordinates of bottom-left corner
    'left_x': 1,
    'bottom_y': 5,

    # width and height
    'width': 10,
    'height': 4,

}

Source: https://www.interviewcake.com/question/python/rectangular-love
'''

from sets import Set

my_rectangle = {'left_x': 1, 'bottom_y': 5, 'width': 10, 'height': 4}
his_rectangle = {'left_x': 23, 'bottom_y': 0, 'width': 2, 'height': 1}
her_rectangle = {'left_x': 2, 'bottom_y': 5, 'width': 10, 'height':4}
their_rectangle = {'left_x': 11, 'bottom_y': 9, 'width': 4, 'height': 9}

def get_corners(rectangle):
    top_left = (rectangle['left_x'], rectangle['bottom_y'] + rectangle['height'])
    top_right = (rectangle['left_x'] + rectangle['width'], top_left[1])
    low_left = (rectangle['left_x'], rectangle['bottom_y'])
    low_right = (top_right[0], rectangle['bottom_y'])
    return top_left, top_right, low_left, low_right

def area(rectangle):
    return (rectangle['width'] * rectangle['height'])


def check_compatibility(date1, date2):
    c1, c2 = get_corners(date1), get_corners(date2)
    all_points = []
    
    for corners in (c1, c2):
        points = Set([])
        left = corners[0][0]
        right = corners[1][0]
        top = corners[0][1]
        bottom = corners[2][1]
        
        for i in range(left, right):
            for j in range(bottom, top):
                points.add((i, j))
        all_points.append(points)
    
    common_pts = len(all_points[0].intersection(all_points[1]))
    
    if common_pts > 1:
        compatibility = common_pts/float(area(date1)) * 100
        return "The compatibility rating is: " + str(compatibility) + '%'
    else:
        return "Sorry. Looks like these two are not a match."



# TEST CASES

# these two rectangles intersect and thus, should have non-zero compatibility
print check_compatibility(my_rectangle, her_rectangle)

# these two rectangles do not intersect and thus, should have zero compatibility
print check_compatibility(my_rectangle, his_rectangle)

# these two rectangles are the same, so they should have compatibility = 100%
print check_compatibility(my_rectangle, my_rectangle)

# these two rectangles only share an edge (not any real area), so they should have zero compatibility
print check_compatibility(my_rectangle, their_rectangle)
