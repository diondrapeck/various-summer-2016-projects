'''
K-Means Binary Image Segmentation by Diondra Peck

A "copy-cat" function of the k-means based image segmentation modules and 
packages in Python and MATLAB. The function takes an image filename of any 
common extension and segments it into two sections by default, but the number
of regions can be specified with a second function argument. The final optional
argument specifies the size the picture will be resized to before processing.

'''

import k_means
import numpy as np
import matplotlib.pyplot as plt
from scipy import misc

def segment(filename, num_regions=2, SIZE=(128, 128)):
    
    # load and resize image
    I = misc.imread(filename)
    I = misc.imresize(I, SIZE)

    # flatten image array into single list
    pixels = (np.ravel(I)).tolist()

    # perform k-means on pixels 
    segments = {}
    while len(segments.keys()) < num_regions:
        segments = k_means.improved_kmeans(pixels, num_regions)


    num_regions += 1
    
    # initiate plots, starting with original image
    sub = "1%i1" % num_regions
    plt.subplot(sub)
    plt.imshow(I, cmap="gray")
    plt.title("Original Image")
    
    ctr = 1
    
    for key in segments.keys():
        sub = "1%i%i" % (num_regions, (ctr + 1),)
        plt.subplot(sub)
        
        hi = max(segments.get(key))
        lo = min(segments.get(key))        
        i = (I <= hi) & (I >= lo)
        
        plt.imshow(i)
        plt.title("Region %i" % ctr)
        ctr += 1
        
    plt.show()