# module for cards games, based on standard 52 card deck

from random import randint, choice

ranks = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King']
suits = ['Clubs', 'Diamonds', 'Hearts', 'Spades']
deck = []

for suit in suits:
    for rank in ranks:
        card = rank + " of " + suit
        deck.append(card)

def shuffle():
    print "Shuffling...\n"
    for i in range(0, 51):
        deck[i], deck[randint(0, 51)] = deck[randint(0, 51)], deck[i]
    return deck

def deal(cards, players):
    if cards % players == 0:
        hands = {}
        dealt = []
        
        for player in range(1, (players + 1)):
            hands.get(player)
            hands[player] = []
            for i in range(0, cards/players):
                card = choice(deck)
                while card in dealt:
                    card = choice(deck)
                hands[player].append(card)
                dealt.append(card)      
    else:
        print "Deck can't be divided evenly amongst %i players" % players
    return hands