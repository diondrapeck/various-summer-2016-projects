
# coding: utf-8

# In[1]:

import pylab as pl
import numpy as np
import time
from datetime import datetime, date, timedelta
from collections import Counter
from bs4 import BeautifulSoup

get_ipython().magic(u'matplotlib inline')

from lxml import etree
parser = etree.XMLParser(recover=True)

tree = etree.parse('messages.htm',parser=parser)
root = tree.getroot()


# In[2]:

# Helper function to filter out tags of a certain class
def filter_class(root, tag, class_):
    results = [] 
    for div in root.findall(tag):
        if 'class' in div.attrib and div.attrib['class'] == class_:
            results.append(div)
    if len(results) == 1: 
        return results[0]
    else: 
        return results

# translates a FB message time into a dateTime object
def getTime(string_time):
    # print string_time
    return datetime.fromtimestamp(time.mktime(time.strptime(string_time[:-3], "%A, %B %d, %Y at %I:%M%p ")))

# returns the difference in mins between two string times 
def minutes_difference(time1, time2):
    return (getTime(time1) - getTime(time2)).seconds / 60.0


# In[3]:

body = root.find('body')
contents = filter_class(body, 'div', 'contents').find('div')
chats = filter_class(contents, 'div', 'thread')


# In[4]:

# counter to enumerate all chats
index = 0
all_names = []

# the main function which gathers character count and time data 
def getInfo(test_case, threshold, up_limit = 5 * 365, low_limit = 0):   
    global index
   
    previous_user = ''
    names =  test_case.text
    print ("%3s" % str(index)) + '  ' + names
    all_names.append(names)
    
    index += 1
    times = {}
    previous_time = None 
    first = True 
    starts = Counter()
    texts = {}

    start = True
        
    for message in test_case:
        # if the div is a message header containing name of person and time
        if start and message.tag == 'div': 
            
            user = message.find('div').find('span').text
            message_time = filter_class(message.find('div'), 'span', 'meta').text
            diff = (datetime.now() - getTime(message_time)).days
            
            if diff > up_limit or diff < low_limit: 
                start = False 
            else: 
                start = True
            
            if first:
                first = False
                previous_time = message_time 
                previous_user = user
                continue
            elif previous_user != user:         
                diff = minutes_difference(previous_time, message_time)
                previous_time = message_time
                if diff < threshold:
                    if previous_user in times: 
                        times[previous_user].append(diff)
                    else: 
                        times[previous_user] = [diff]
                elif diff > 120: 
                    starts[previous_user] += 1
                previous_user = user
                
        if message.tag == 'p':
            if texts.has_key(user):
                texts[user].append(message.text)
            else:
                texts[user] = [message.text]
            
    return times, starts, texts

# calculates the total character count and average response time 
def modify_dicts(t, s, c):  
    for item in t:
        t[item] = np.mean(np.array(t[item]))
    return t,s,c


# In[5]:

# wraps together getInfo and modify_dicts

# change the 120 below to modify the upper bound on a message response time 
def compile_stats(chat_num, up_limit = 5*365, low_limit = 0):
    t, s, c = getInfo(chats[chat_num], 45, up_limit = up_limit, low_limit = low_limit)
    return modify_dicts(t,s,c)


# In[6]:

t_global = [] 
s_global = []
c_global = []

print "Use this to find the chat number: "

for i in range(len(chats)):  #len(chats))
    t,s,c = compile_stats(i)
    t_global.append(t)
    s_global.append(s)
    c_global.append(c)

index = 0 


# In[7]:

# Makes 3 plots (1) character count, (2) average response time, (3) initiating conversation
def plot(chat_number):
        
    # prints plot of cummulative character count
    def character_count(messages): 
        character_count = []
        persons = messages[chat_number].keys()

        for person in persons:
            print person
            lengths = []
            word_count = 0
            messages = c_global[chat_number][person]

            for message in messages:
                if message is None:
                    continue
                lengths.append(len(message))

            character_count.append(sum(lengths)/float(len(messages)))
        return character_count
    
    
    avg_word_count = character_count(c_global)
    names = c_global[chat_number].keys()
    Z = np.arange(len(names))
    pl.bar(Z, np.array(avg_word_count), align='center', width=0.5, color = 'g')
    pl.xticks(Z, names, rotation = 90)
    pl.title('Average Character Count')
    pl.show()

    
    # prints plot of average response time
    d = t_global[chat_number]
    X = np.arange(len(d))
    pl.bar(X, np.array(d.values()), align='center', width=0.5, color = 'r')
    pl.xticks(X, d.keys(), rotation = 90)
    pl.title('Average Response Time in Minutes')
    pl.show()

    # prints plot of conversation initiations
    initiations = s_global[chat_number].values()
    names = s_global[chat_number].keys()
    Y = np.arange(len(names))
    pl.bar(Y, np.array(initiations), align='center', width=0.5, color = 'b')
    pl.xticks(Y, names, rotation = 90)
    pl.title('Number of Conversation Initiations')
    pl.show()
    
    


# In[8]:

plot(94)


# # Bonus Hack!
# 
# ### I created a Markov Model Impersonator of my Facebook friends and a function to evaluate the accuracy of the impersonator using the PositiveNaiveBayesClassifier module in the nltk (Natural Language Toolkit) library.

# In[ ]:

# helper functions

# returns a list of all of the indexes of conversations involving the user
def compile_chat_nums(user):
    all_conversations = []
    
    for i in range(len(chats)):
        persons = c_global[i].keys()
        
        if user not in persons:
            continue  
        if chats[i] is None:
            continue
        all_conversations.append(i)
            
    return all_conversations

# returns a dictionary of all the user's words (words are keys and words that have immediately followed that word are its values)
# and a list of all the messages sent by the user
def compile_messages(user, chat_list):
    import re
    
    all_words = {}
    
    for chat in chat_list:
        messages = c_global[chat][user]
        
        messages = [message for message in messages if message is not None]
        
        for message in messages:
            if message is None:
                continue
            
            # removes all punctuation from messages (except apostrophes)
            message = [re.sub(ur"[^\w\d'\s]+", '', m) for m in message.split()]
            
            for word in message:
                if (message.index(word) + 1) >= len(message):
                    continue
                
                if all_words.has_key(word):
                    all_words[word].append(message[message.index(word) + 1])
                else:
                    all_words[word] = [message[message.index(word) + 1]]
    return all_words, messages
          
# returns the average word count of the user's messages
def individual_count(chat_number, user): 
    word_count = 0
    num_messages = 0
    
    for chat in chat_number:
        persons = c_global[chat].keys()

        for person in persons:
            if user == person:
                messages = c_global[chat][person]

                for message in messages:
                    if message is None:
                        continue
                    num_messages += 1
                    word_count += len(message)
            else:
                continue
    return round(word_count/float(num_messages))

# returns an impersonation of the user with the length of their average word count
def markov_impersonator(user):
    import random
    
    message_length = 0
    chat_list = compile_chat_nums(user)
    dictionary = compile_messages(user, chat_list)[0]
    word_limit = individual_count(chat_list, user)
    impersonation = ""
    
    begin = random.choice(dictionary.keys())
    
    if dictionary.has_key(begin):
        impersonation += begin
        message_length += 1
    
    while message_length < word_limit:
        next_word = random.choice(dictionary[begin])

        if dictionary.has_key(next_word) and next_word != begin:
            impersonation += (" " + next_word)
            message_length += 1
            begin = next_word
            
    return impersonation.lower().capitalize() + "."

print markov_impersonator("Josh Stallings")


# In[ ]:

# compares 10 sentences generated by markov_impersonator to 10 of the user's actual sentences to calculate the model's accuracy
def markov_evaluator(user):
    import random
    from nltk.classify import PositiveNaiveBayesClassifier

    user_sentences = random.sample(compile_messages(user, compile_chat_nums(user))[1], 10)
    common_sentences = ['The President did not comment','I lost the keys','The team won the game','Sara has two kids',
                        'The ball went off the court', 'They took math together', 'The show is over', 
                        'The water is cold', "I like pudding", "There is a cat"]
    generated_sentences = [markov_impersonator(user) for i in range(10)]

    def features(sentence):
        words = sentence.lower().split()
        return dict(('contains(%s)' % w, True) for w in words)

    positive_featuresets = list(map(features, user_sentences))
    unlabeled_featuresets = list(map(features, common_sentences))

    classifier = PositiveNaiveBayesClassifier.train(positive_featuresets, unlabeled_featuresets)
    results = [classifier.classify(features(i)) for i in generated_sentences]
    accuracy = sum(results)/10.0 * 100
    
    return "The Markov Model Facebook Impersonator is " + str(accuracy) + "% correct for " + user + "."

print markov_evaluator("Josh Stallings")


# In[ ]:



